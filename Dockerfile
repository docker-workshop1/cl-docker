FROM node:14-alpine as src
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY ./ /usr/src/app

FROM src as dev
EXPOSE 3000
CMD npm i && \
    npm run dev

FROM src
RUN npm i && npm run build
EXPOSE 3000
CMD npm run start
