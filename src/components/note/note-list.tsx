import React, {useState} from 'react';
import {Note as NoteModel} from '../../models/note'
import Note from "./note";

const NoteList = () => {
    const [notes, setNotes] = useState([
            {
                id: 1,
                title: "Note 1",
                content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, minus.",
                tags: []
            } as NoteModel,
            {
                id: 2,
                title: "Note 2",
                content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum ipsa laudantium magni maxime quae quisquam quod quos tempore! Ab accusamus, accusantium ad aliquam animi aspernatur aut beatae blanditiis dolore eaque earum enim eos eum ex facilis fugiat impedit ipsam iure maiores non omnis, qui quidem quo quod quos, rem repudiandae similique sint tempore vitae. Facilis iusto pariatur quo rem, reprehenderit sint veniam. Alias ea odio ullam veritatis? Ad debitis dicta dolor eligendi excepturi inventore, iure non odio odit optio pariatur perspiciatis placeat quasi recusandae reiciendis sunt ut voluptatem. Accusamus aliquam, aliquid beatae fuga odio pariatur quos repellat soluta velit voluptas?",
                tags: []
            } as NoteModel,
        ]
    )

    return (
        <>
            {notes.map(note => <Note note={note}/>)}
        </>
    );
};

export default NoteList;