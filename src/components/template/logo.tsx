import {Typography} from "@material-ui/core";
import Link from "next/link";
import React from "react";


export const Logo = () =>
        <Link href={"/"}>
            <Typography variant={"h6"} color={"textPrimary"} style={{padding:"20px 0", cursor:"pointer"}}>YOUR NOTES</Typography>
        </Link>
